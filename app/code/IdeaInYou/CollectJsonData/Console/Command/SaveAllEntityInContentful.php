<?php

namespace IdeaInYou\CollectJsonData\Console\Command;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use IdeaInYou\CollectJsonData\Model\SyncToContentful;

/**
 * Class SaveAllEntityInContentful
 * @package IdeaInYou\CollectJsonData\Console\Command
 */
class SaveAllEntityInContentful extends Command
{
    private SyncToContentful $syncToContentful;

    /**
     * @param SyncToContentful $syncToContentful
     */
    public function __construct(
        SyncToContentful $syncToContentful
    ) {
        parent::__construct();
        $this->syncToContentful = $syncToContentful;
    }

    /**
     * Initialization of the command.
     */
    protected function configure()
    {
        $this->setName('save:allentry');
        $this->setDescription('Save and change all entry on contentful');
        parent::configure();
    }

    /**
     * CLI command description.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     * @throws LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        try {
            $this->syncToContentful->syncAllEntity();
            $output->writeln("Entity save success");
        } catch (NoSuchEntityException $e) {
            $output->writeln("Entity save error", $e);
        }
    }

}

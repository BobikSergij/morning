<?php

namespace IdeaInYou\CollectJsonData\Plugin\Product\Model\ResourceModel;

use IdeaInYou\CollectJsonData\Api\ValidateContentfulInterface;
use Magento\Catalog\Model\ResourceModel\Product;


class ProductBeforeDelete
{
    const TYPE = 'product';

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful
    ) {
        $this->validateContentful = $validateContentful;
    }

    /**
     * @param Product $subject
     * @return void
     */
    public function beforeDelete(Product $subject)
    {
        $this->validateContentful->validator(self::TYPE);
    }
}

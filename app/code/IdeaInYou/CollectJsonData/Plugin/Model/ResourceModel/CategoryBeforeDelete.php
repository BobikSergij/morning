<?php

namespace IdeaInYou\CollectJsonData\Plugin\Model\ResourceModel;

use IdeaInYou\CollectJsonData\Api\ValidateContentfulInterface;
use Magento\Catalog\Model\ResourceModel\Category;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Registry;

class CategoryBeforeDelete
{
    const TYPE = 'category';

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @var Registry
     */
    private Registry $registry;

    /**
     * @param ValidateContentfulInterface $validateContentful
     * @param Registry $registry
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful,
        Registry $registry
    ) {
        $this->validateContentful = $validateContentful;
        $this->registry = $registry;
    }

    /**
     * @param Category $subject
     * @param AbstractModel $object
     * @return void
     */
    public function beforeDelete(Category $subject, AbstractModel $object)
    {
        $this->validateContentful->validator(self::TYPE);
        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $objectManager = $_objectManager->create('Magento\Catalog\Model\Category')->load($object->getEntityId());
        $url = $objectManager->getUrlKey();
        $key = (string)$objectManager->getEntityId();
        $this->registry->register($key, $url);
    }
}

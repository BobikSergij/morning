<?php

namespace IdeaInYou\CollectJsonData\Plugin\Model\ResourceModel;

use Magento\Catalog\Model\Category;
use IdeaInYou\CollectJsonData\Model\ContentfulConfig\UpdateCategoryMenu;

class SaveAfterCategoryMove
{
    private UpdateCategoryMenu $updateCategoryMenu;

    /**
     * @param UpdateCategoryMenu $updateCategoryMenu
     */
    public function __construct(
        UpdateCategoryMenu $updateCategoryMenu
    ) {
        $this->updateCategoryMenu = $updateCategoryMenu;
    }

    /**
     * @param Category $subject
     * @param $result
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterMove(Category $subject, $result)
    {
        $this->updateCategoryMenu->updateCategoryTree($subject);
        return $result;
    }

}

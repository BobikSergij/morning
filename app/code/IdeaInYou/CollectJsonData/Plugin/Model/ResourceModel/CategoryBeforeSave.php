<?php

namespace IdeaInYou\CollectJsonData\Plugin\Model\ResourceModel;

use IdeaInYou\CollectJsonData\Api\ValidateContentfulInterface;
use Magento\Catalog\Model\ResourceModel\Category;

class CategoryBeforeSave
{
    const TYPE = 'category';

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful
    ) {
        $this->validateContentful = $validateContentful;
    }

    /**
     * @param Category $subject
     * @return void
     */
    public function beforeSave(Category $subject)
    {
        $this->validateContentful->validator(self::TYPE);
    }
}

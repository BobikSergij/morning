<?php

namespace IdeaInYou\CollectJsonData\Plugin\Model\ResourceModel;

use IdeaInYou\CollectJsonData\Api\ValidateContentfulInterface;
use Magento\Catalog\Model\Category;

class BeforeCategoryMove
{
    const TYPE = 'category';

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful
    ) {
        $this->validateContentful = $validateContentful;
    }

    /**
     * @param Category $subject
     * @return void
     */
    public function beforeMove(Category $subject)
    {
        $this->validateContentful->validator(self::TYPE);
    }
}

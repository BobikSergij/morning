<?php

namespace IdeaInYou\CollectJsonData\Plugin\Page\Model\ResourceModel;

use Magento\Cms\Model\ResourceModel\Page;
use Magento\Framework\Model\AbstractModel;
use IdeaInYou\CollectJsonData\Api\ValidateContentfulInterface;
use IdeaInYou\CollectJsonData\Model\Page\PageUpdate;

class PageDelete
{
    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @var PageUpdate
     */
    private PageUpdate $pageUpdate;

    /**
     * @param ValidateContentfulInterface $validateContentful
     * @param PageUpdate $pageUpdate
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful,
        PageUpdate $pageUpdate
    ) {
        $this->validateContentful = $validateContentful;
        $this->pageUpdate = $pageUpdate;
    }

    /**
     * @param Page $subject
     * @param $result
     * @param AbstractModel $object
     * @return mixed
     */
    public function afterDelete(Page $subject, $result, AbstractModel $object)
    {
        if (!$this->validateContentful->isEnable()) return $result;
        $this->pageUpdate->updatePageEntryContentful($object);
        return $result;
    }
}

<?php

namespace IdeaInYou\CollectJsonData\Plugin\Page\Model\ResourceModel;

use Magento\Cms\Model\ResourceModel\Page;
use Magento\Framework\Model\AbstractModel;
use IdeaInYou\CollectJsonData\Model\ConnectToContentful\UrlRewrites;
use IdeaInYou\CollectJsonData\Api\ValidateContentfulInterface;

class PageSave
{
    /**
     * @var UrlRewrites
     */
    private UrlRewrites $urlRewrites;

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @param UrlRewrites $urlRewrites
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        UrlRewrites                 $urlRewrites,
        ValidateContentfulInterface $validateContentful
    ) {
        $this->urlRewrites = $urlRewrites;
        $this->validateContentful = $validateContentful;
    }

    /**
     * @param Page $subject
     * @param $result
     * @param AbstractModel $object
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterSave(Page $subject, $result, AbstractModel $object)
    {
        if (!$this->validateContentful->isEnable()) return $result;
        $this->urlRewrites->addUrlToContentful($object, 'page');
        return $result;
    }

}

<?php

namespace IdeaInYou\CollectJsonData\Plugin\Page\Model\ResourceModel;

use IdeaInYou\CollectJsonData\Api\ValidateContentfulInterface;
use Magento\Cms\Model\ResourceModel\Page;

class PageBeforeDelete
{
    const TYPE = 'urlRewrite';

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful
    ) {
        $this->validateContentful = $validateContentful;
    }

    /**
     * @param Page $subject
     * @return void
     */
    public function beforeDelete(Page $subject)
    {
        $this->validateContentful->validator(self::TYPE);
    }
}

<?php

namespace IdeaInYou\CollectJsonData\Plugin;

use Magento\Config\Model\Config as MagentoConfig;
use IdeaInYou\CollectJsonData\Model\ConnectToContentful\ConfigurationData;
use IdeaInYou\CollectJsonData\Api\ValidateContentfulInterface;

class ConfigurationSavePlugin
{

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;
    private ConfigurationData $configurationData;


    /**
     * @param ConfigurationData $configurationData
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        ConfigurationData           $configurationData,
        ValidateContentfulInterface $validateContentful
    ) {
        $this->validateContentful = $validateContentful;
        $this->configurationData = $configurationData;
    }

    /**
     * @param MagentoConfig $subject
     * @param \Closure $proceed
     * @return mixed
     */
    public function aroundSave(MagentoConfig $subject, \Closure $proceed)
    {
        if ($this->validateContentful->isEnable()) {
            $this->configurationData->syncSectionFields($subject->getData());
        }
        return $proceed();
    }
}

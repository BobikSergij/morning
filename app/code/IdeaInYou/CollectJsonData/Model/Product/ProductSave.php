<?php

namespace IdeaInYou\CollectJsonData\Model\Product;

use IdeaInYou\CollectJsonData\Model\Product\ProductsCollection;

class ProductSave
{

    /**
     * @var \IdeaInYou\CollectJsonData\Model\Product\ProductsCollection
     */
    private \IdeaInYou\CollectJsonData\Model\Product\ProductsCollection $productsCollection;

    /**
     * @param ProductsCollection $productsCollection
     */
    public function __construct(
        ProductsCollection $productsCollection
    ) {
        $this->productsCollection = $productsCollection;
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function saveAllEntity($type, $store){
        $this->productsCollection->saveAllProductsToContentful($type, $store);
    }

}

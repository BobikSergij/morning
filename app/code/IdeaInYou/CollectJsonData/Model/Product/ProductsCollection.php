<?php

namespace IdeaInYou\CollectJsonData\Model\Product;

use IdeaInYou\CollectJsonData\Model\ConnectToContentful\ContentfulData;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\State;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\FilterBuilder;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;
use IdeaInYou\CollectJsonData\Model\ConnectToContentful\DataProductPrepare;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class ProductsCollection
 * @package IdeaInYou\CollectJsonData\Model\Product
 */
class ProductsCollection
{
    /**
     * @var ProductRepository
     */
    protected ProductRepository $productRepository;
    /**
     * @var SearchCriteriaInterface
     */
    protected SearchCriteriaInterface $searchCriteria;
    /**
     * @var FilterGroup
     */
    protected FilterGroup $filterGroup;
    /**
     * @var FilterBuilder
     */
    protected FilterBuilder $filterBuilder;
    /**
     * @var Status
     */
    protected Status $productStatus;
    /**
     * @var Visibility
     */
    protected Visibility $productVisibility;
    protected DataProductPrepare $helperProduct;

    /**
     * @param ProductRepository $productRepository
     * @param SearchCriteriaInterface $criteria
     * @param FilterGroup $filterGroup
     * @param FilterBuilder $filterBuilder
     * @param Status $productStatus
     * @param Visibility $productVisibility
     * @param DataProductPrepare $helperProduct
     * @param State $state
     */
    public function __construct(
        ProductRepository $productRepository,
        SearchCriteriaInterface $criteria,
        FilterGroup $filterGroup,
        FilterBuilder $filterBuilder,
        Status $productStatus,
        Visibility $productVisibility,
        DataProductPrepare $helperProduct,
        State $state
    ) {
        $this->productRepository = $productRepository;
        $this->searchCriteria = $criteria;
        $this->filterGroup = $filterGroup;
        $this->filterBuilder = $filterBuilder;
        $this->productStatus = $productStatus;
        $this->productVisibility = $productVisibility;
        $this->helperProduct = $helperProduct;
    }

    /**
     * @throws NoSuchEntityException
     */
    public function saveAllProductsToContentful($type, $store)
    {
        $products = $this->getAllProducts();
        $count = 1;
        $total = count($products);

        /** @var $product Product */
        foreach ($products as $product) {
            $this->helperProduct->prepareProductContentfulData($product);
            $count++;
        }
    }

    /**
     * @return ProductInterface[]
     */
    protected function getAllProducts(): array
    {
        $this->filterGroup->setFilters([
            $this->filterBuilder
                ->setField('status')
                ->setConditionType('in')
                ->setValue($this->productStatus->getVisibleStatusIds())
                ->create(),
            $this->filterBuilder
                ->setField('visibility')
                ->setConditionType('in')
                ->setValue($this->productVisibility->getVisibleInSiteIds())
                ->create(),
        ]);

        $this->searchCriteria->setFilterGroups([$this->filterGroup]);
        $products = $this->productRepository->getList($this->searchCriteria);

        return $products->getItems();
    }
}

<?php

namespace IdeaInYou\CollectJsonData\Model\ConnectToContentful\Addition;

use IdeaInYou\CollectJsonData\Api\ValidateContentfulInterface;
use Magento\Store\Model\StoreManagerInterface;
use IdeaInYou\CollectJsonData\Model\ConnectToContentful\ContentfulData;
use IdeaInYou\CollectJsonData\Api\SyncCustomAdditionToContentfulInterface;

class SyncCustomAdditionToContentful implements SyncCustomAdditionToContentfulInterface
{
    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var ContentfulData
     */
    private ContentfulData $contentfulData;

    /**
     * @param ValidateContentfulInterface $validateContentful
     * @param StoreManagerInterface $storeManager
     * @param ContentfulData $contentfulData
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful,
        StoreManagerInterface       $storeManager,
        ContentfulData              $contentfulData
    ) {
        $this->validateContentful = $validateContentful;
        $this->storeManager = $storeManager;
        $this->contentfulData = $contentfulData;
    }

    /**
     * @param $type
     * @param $object
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function prepareSyncContentfulData($type, $object)
    {
        $fieldsQuery = 'entity_id';
        $entityId = $object->getId();
        $customEntry = $this->validateContentful->validateContentfulEntries($type, $fieldsQuery, $entityId);
        if (!$customEntry) return;

        $entries = $customEntry['entries'];
        $environment = $customEntry['environment'];
        if ($entries->getTotal() > 0) {
            $entry = $this->contentfulData->getEntry($entries, $environment);
            $this->checkStoreView($entry, $object);
            $entry->update();
        } else {
            if (!$this->contentfulData->isCreateEntryEnable($object, $type)) return;
            $entry = new \Contentful\Management\Resource\Entry($type);
            $this->checkStoreView($entry, $object);
            $environment->create($entry);
        }
        $entry->publish();
    }


    /**
     * @param $entry
     * @param $object
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function checkStoreView($entry, $object)
    {
        $data = $object->getData();
        if ($object->getStoreId() == '0') {
            $stores = $this->contentfulData->getAllStores();
            foreach ($stores as $store) {
                $locale = $this->contentfulData->getStoreLangByCode($store->getCode());
                if (!$locale) continue;
                $this->setEntryFields($entry, $locale, $data);
            }
        } else {
            $locale = $this->contentfulData->getStoreLangByCode(
                $this->storeManager->getStore($object->getStoreId())->getCode()
            );
            if (!$locale) return;
            $this->setEntryFields($entry, $locale, $data);
        }
    }

    /**
     * @param $entry
     * @param $locale
     * @param $data
     * @return void
     */
    protected function setEntryFields($entry, $locale, $data)
    {
        $entityId = (int)$data['entity_id'];
        $isEnable = (bool)$data['is_enable'];
        foreach ($this->contentfulData->getAllStores() as $store){
            $loc = $this->contentfulData->getStoreLangByCode($store->getCode());
            if (!$loc) continue;
            $entry->setField('entity_id', $loc, $entityId);
        }
        $entry->setField('code', $locale, $data['code']);
        $entry->setField('visible', $locale, $isEnable);
        $entry->setField('jsonData', $locale, $data);
    }

    /**
     * @param $type
     * @param $object
     * @return void
     */
    public function prepareToDeleteContentfulData($type, $object)
    {
        $fieldsQuery = 'entity_id';
        $entityId = $object->getId();
        $customEntry = $this->validateContentful->validateContentfulEntries($type, $fieldsQuery, $entityId);
        if (!$customEntry) return;

        $entries = $customEntry['entries'];
        $environment = $customEntry['environment'];
        if ($entries->getTotal() == 0) {
            return;
        } else {
            $entry = $this->contentfulData->getEntry($entries, $environment);
            $this->contentfulData->deleteEntry($entry);
        }
    }
}

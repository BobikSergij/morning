<?php

namespace IdeaInYou\CollectJsonData\Model\ConnectToContentful;

use Contentful\Management\Proxy\Extension\SpaceProxyExtension;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Locale\Resolver;
use Magento\Customer\Model\Session as Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use IdeaInYou\CollectJsonData\Model\GetDataLocal;

/**
 * Class ContentfulData
 * @package IdeaInYou\CollectJsonData\Model\ConnectToContentful
 */
class ContentfulData
{

    use SpaceProxyExtension;

    /**
     *
     */
    const XML_CONTENTFUL_ENABLE = 'contentfulconfigsync/contentful_general/contentful_enable';
    /**
     *
     */
    const XML_CONTENT_MANAGEMENT_API_KEY = 'contentfulconfigsync/contentful_general/contentful_api';
    /**
     *
     */
    const XML_SPACE_ID = 'contentfulconfigsync/contentful_general/contentful_space_id';

    /**
     *
     */
    const ENVIRONMENT_ID = 'contentfulconfigsync/contentful_general/contentful_environment_id';

    /**
     *
     */
    const XML_STORE_VIEW_LOCALE = 'contentfulconfigsync/locale/row';

    /**
     *
     */
    const DEFAULT_STORE_VIEW = 'contentfulconfigsync/locale/default_store_view_contentful';

    /**
     *
     */
    const CONTENT_CONFIGURATION_TYPE_ID = 'configuration';

    /**
     *
     */
    const CONTENT_URLREWRITE_TYPE_ID = 'urlRewrite';
    /**
     *
     */
    const CONTENT_CATEGORY_TYPE_ID = 'category';
    /**
     *
     */
    const CONTENT_PRODUCT_TYPE_ID = 'product';
    /**
     *
     */
    const CONTENT_MENU_TYPE_ID = 'menu';


    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var
     */
    private $storeManager;

    /**
     * @var Resolver
     */
    protected Resolver $resolver;

    /**
     * @var Session
     */
    protected Session $customerSession;

    /**
     * @var GetDataLocal
     */
    private GetDataLocal $getDataLocal;


    /**
     * @param Resolver $resolver
     * @param Session $customerSession
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param GetDataLocal $getDataLocal
     */
    public function __construct(
        Resolver              $resolver,
        Session               $customerSession,
        ScopeConfigInterface  $scopeConfig,
        StoreManagerInterface $storeManager,
        GetDataLocal          $getDataLocal
    ) {
        $this->resolver = $resolver;
        $this->customerSession = $customerSession;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->getDataLocal = $getDataLocal;
    }

    /**
     * @return \Contentful\Core\Resource\ResourceArray
     */
    public function getEnvironmentIds()
    {
        return $this->getEnvironments();
    }

    /**
     * @return string
     */
    public function getEnvironmentId()
    {
        return $this->scopeConfig->getValue(self::ENVIRONMENT_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getContentManagementApiKey()
    {
        return $this->scopeConfig->getValue(self::XML_CONTENT_MANAGEMENT_API_KEY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getSpaceId()
    {
        return $this->scopeConfig->getValue(self::XML_SPACE_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getEnableId()
    {
        return $this->scopeConfig->getValue(self::XML_CONTENTFUL_ENABLE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getStoreViewLocales()
    {
        return $this->scopeConfig->getValue(self::XML_STORE_VIEW_LOCALE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }


    public function getDefaultStoreView()
    {
        return $this->scopeConfig->getValue(self::DEFAULT_STORE_VIEW,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @throws NoSuchEntityException
     */
    public function getStoreCodeById($storeId): string
    {
        return $this->storeManager->getStore($storeId)->getCode();
    }

    /**
     *  Return default language code for Contentful
     * @return false|string
     */
    public function getDefaultISOCode()
    {
        return $this->getStoreLangByCode($this->getDefaultStoreView());
    }


    /**
     *  Return all ISO of language code for Contentful
     * @return array
     * @throws NoSuchEntityException
     */
    public function getAllISOCode(): array
    {
        $arr = [];
        try {
            $locals = $this->getDataLocal->getLocal();
        } catch (\Exception $e) {
        }

        foreach ($locals as $key => $local) {
            $arr[$key] = $local;
        }
        return $arr;
    }

    /**
     * @param $object
     * @return string
     * @throws NoSuchEntityException
     */
    public function getStoreCode($object): string
    {
        if (!empty($object->getIdentifier())) {
            $storeId = $object->getStoreId()[0];
        } else {
            $storeId = $object->getStoreId();
        }
        return $this->storeManager->getStore($storeId)->getCode();
    }

    /**
     * @param $entries
     * @param $environment
     * @return mixed
     */
    public function getEntry($entries, $environment)
    {
        try {
            return $environment->getEntry(
                $entries->getItems()[0]->jsonSerialize()['sys']->getId());
        } catch (\Exception $e) {
        }

    }

    /**
     * @param $entry
     * @return void
     */
    public function deleteEntry($entry)
    {
        $entry->unpublish();
        $entry->delete();
    }

    /**
     * @param $langCode
     * @return false|mixed
     */
    function getStoreByLanguageCode($langCode)
    {
        $stores = $this->storeManager->getStores();

        foreach ($stores as $store) {
            if (str_contains($store->getCode(), $langCode)) return $store;
        }

        return false;
    }

    /**
     * @return false|mixed
     */
    function getEnStore()
    {
        return $this->getStoreByLanguageCode('en');
    }

    /**
     * @return false|mixed
     */
    function getRuStore()
    {
        return $this->getStoreByLanguageCode('ru');
    }

    /**
     * @return false|mixed
     */
    function getUaStore()
    {
        return $this->getStoreByLanguageCode('ua');
    }

    /**
     * @param $code
     * @return false|string
     * @throws NoSuchEntityException
     */
    function getStoreLangByCode($code)
    {
        if ($code == 'admin') {
            return $this->getStoreLangByCode($this->getDefaultStoreView());
        }
        $locals = $this->getDataLocal->getLocal();
        foreach ($locals as $key => $local) {
            $true = str_contains($code, $key);
            if ($true) {
                return $local;
            }
        }

        return false;
    }

    /**
     * @return \Magento\Store\Api\Data\StoreInterface[]
     */
    public function getAllStores(): array
    {
        return $this->storeManager->getStores();
    }

    /**
     * @param $object
     * @param $type
     * @return bool|void
     * @throws NoSuchEntityException
     */
    public function isCreateEntryEnable($object, $type = null)
    {
        try {
            $storeLocales = $this->getStoreViewLocales();
            if ($type === null || $type != 'page') {
                $storeId = $object->getStoreId();
            } else {
                $storeId = $object->getStoreId()[0];
            }
            $localeCode = $this->getStoreCodeById($storeId);
            if ($localeCode == 'admin') return true;
            return (str_contains($storeLocales, $localeCode));
        } catch (\Exception $e) {
        }
    }
}

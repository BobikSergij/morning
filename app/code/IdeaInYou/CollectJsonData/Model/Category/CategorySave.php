<?php

namespace IdeaInYou\CollectJsonData\Model\Category;

use IdeaInYou\CollectJsonData\Model\Category\CategoriesCollection;

class CategorySave
{
    /**
     * @var \IdeaInYou\CollectJsonData\Model\Category\CategoriesCollection
     */
    private \IdeaInYou\CollectJsonData\Model\Category\CategoriesCollection $categoriesCollection;

    /**
     * @param CategoriesCollection $categoriesCollection
     */
    public function __construct(
        CategoriesCollection $categoriesCollection
    ) {
        $this->categoriesCollection = $categoriesCollection;
    }

    /**
     * @param $store
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function saveAllEntity($type, $store){
        $this->categoriesCollection->saveAllCategoriesToContentful($type, $store);
    }

}

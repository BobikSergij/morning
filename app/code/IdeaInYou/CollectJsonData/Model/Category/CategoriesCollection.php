<?php

namespace IdeaInYou\CollectJsonData\Model\Category;

use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\ResourceModel\Category\Collection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use IdeaInYou\CollectJsonData\Model\ConnectToContentful\UrlRewrites;

/**
 * Class CategoriesCollection
 * @package IdeaInYou\CollectJsonData\Model\Category
 */
class CategoriesCollection
{
    /**
     * @var CollectionFactory
     */
    protected CollectionFactory $categoryCollection;
    /**
     * @var StoreManagerInterface
     */
    protected StoreManagerInterface $storeManager;
    /**
     * @var UrlRewrites
     */
    protected UrlRewrites $urlRewrites;

    /**
     * @param CollectionFactory $categoryCollection
     * @param StoreManagerInterface $storeManager
     * @param UrlRewrites $urlRewrites
     */
    public function __construct(
        CollectionFactory     $categoryCollection,
        StoreManagerInterface $storeManager,
        UrlRewrites           $urlRewrites
    ) {
        $this->categoryCollection = $categoryCollection;
        $this->storeManager = $storeManager;
        $this->urlRewrites = $urlRewrites;
    }

    /**
     * @param $type
     * @param $store
     * @return void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function saveAllCategoriesToContentful($type, $store)
    {
        $categories = $this->getCategoriesPerStore($store)->getItems();
        /** @var $category Category */
        foreach ($categories as $category) {
            if (!($category->getEntityId() == 2)) {
                $category->setStoreId($store->getId());
                $this->urlRewrites->addUrlToContentful($category, $type);
                $this->urlRewrites->addUrlToContentfulCategories($category);
            }
        }
    }

    /**
     * @param $store
     * @return Collection
     * @throws LocalizedException
     */
    protected function getCategoriesPerStore($store): Collection
    {
        return $this->categoryCollection->create()
            ->addAttributeToSelect('*')
            ->setStore($store)
            ->addAttributeToFilter('is_active', '1');
    }
}

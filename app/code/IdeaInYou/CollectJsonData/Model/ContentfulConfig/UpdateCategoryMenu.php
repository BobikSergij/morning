<?php

namespace IdeaInYou\CollectJsonData\Model\ContentfulConfig;

use IdeaInYou\CollectJsonData\Model\ConnectToContentful\ContentfulData;
use IdeaInYou\CollectJsonData\Block\CustomCategoryCollection;
use IdeaInYou\CollectJsonData\Api\ValidateContentfulInterface;
use Magento\Catalog\Model\ResourceModel\Category as ResourceCategory;

class UpdateCategoryMenu
{
    /**
     * @var CustomCategoryCollection
     */
    private CustomCategoryCollection $collection;

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @var ResourceCategory
     */
    private ResourceCategory $resourceCategory;

    /**
     * @var ContentfulData
     */
    private ContentfulData $contentfulData;

    /**
     * @param CustomCategoryCollection $collection
     * @param ValidateContentfulInterface $validateContentful
     * @param ResourceCategory $resourceCategory
     * @param ContentfulData $contentfulData
     */
    public function __construct(
        CustomCategoryCollection    $collection,
        ValidateContentfulInterface $validateContentful,
        ResourceCategory            $resourceCategory,
        ContentfulData                      $contentfulData
    ) {
        $this->collection = $collection;
        $this->validateContentful = $validateContentful;
        $this->resourceCategory = $resourceCategory;
        $this->contentfulData = $contentfulData;
    }


    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getTree()
    {
        return $this->collection->getTree();
    }


    /**
     * @param $object
     * @return mixed|void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function updateCategoryTree($object)
    {
//        if ($object->getStore()->getId() == 0) return;

        $storeCode = $object->getStore()->getCode();
        $locale = $this->contentfulData->getStoreLangByCode($storeCode);
        if (!$locale) return;



        $contentTypeId = $this->contentfulData::CONTENT_MENU_TYPE_ID;
        $fieldsQuery = 'code';
        $value = 'top-menu';
        $customEntry = $this->validateContentful->validateContentfulEntries($contentTypeId, $fieldsQuery, $value);
        if ($customEntry) {
            $tree = $this->getTree()[0]['children'];
            $entries = $customEntry['entries'];
            $environment = $customEntry['environment'];
            $entry = $this->contentfulData->getEntry($entries, $environment);
            $entry->setField('jsonData', $locale, $tree);
            $entry->update();
            $entry->publish();
        } else {
            $this->resourceCategory->rollBack();
        }
    }

}

<?php

namespace IdeaInYou\CollectJsonData\Model\Page;

use IdeaInYou\CollectJsonData\Model\Page\PageCollection;

class PageSave
{
    /**
     * @var \IdeaInYou\CollectJsonData\Model\Page\PageCollection
     */
    private \IdeaInYou\CollectJsonData\Model\Page\PageCollection $pageCollection;

    /**
     * @param PageCollection $pageCollection
     */
    public function __construct(
        PageCollection $pageCollection
    ) {
        $this->pageCollection = $pageCollection;
    }

    /**
     * @param $type
     * @param $store
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function saveAllEntity($type, $store){
        $this->pageCollection->saveAllPagesToContentful($type, $store);
    }

}

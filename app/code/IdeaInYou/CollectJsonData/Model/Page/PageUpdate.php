<?php

namespace IdeaInYou\CollectJsonData\Model\Page;

use Elasticsearch\Endpoints\Count;
use IdeaInYou\CollectJsonData\Model\ConnectToContentful\ContentfulData;
use IdeaInYou\CollectJsonData\Api\ValidateContentfulInterface;
use IdeaInYou\CollectJsonData\Model\ConnectToContentful\UrlRewrites;

class PageUpdate
{
    const Type = 'page';
    private ContentfulData $contentfulData;
    private ValidateContentfulInterface $validateContentful;
    private UrlRewrites $urlRewrites;

    /**
     * @param ContentfulData $contentfulData
     * @param ValidateContentfulInterface $validateContentful
     * @param UrlRewrites $urlRewrites
     */
    public function __construct(
        ContentfulData              $contentfulData,
        ValidateContentfulInterface $validateContentful,
        UrlRewrites                 $urlRewrites
    )
    {
        $this->contentfulData = $contentfulData;
        $this->validateContentful = $validateContentful;
        $this->urlRewrites = $urlRewrites;
    }

    public function updatePageEntryContentful($object)
    {
        $contentTypeId = $this->contentfulData::CONTENT_URLREWRITE_TYPE_ID;
        $entityId = $object->getData('identifier');
        $fieldsQuery = 'urlKey';
        $fieldsQueryType = 'type';
        $customEntry = $this->validateContentful->validateContentfulEntries(
            $contentTypeId,
            $fieldsQuery,
            $entityId,
            $fieldsQueryType,
            self::Type);
        if (!$customEntry) return;
        $entries = $customEntry['entries'];
        $environment = $customEntry['environment'];

        $entry = $this->contentfulData->getEntry($entries, $environment);
        $locale = $this->contentfulData->getStoreLangByCode($this->contentfulData->getStoreCode($object));
        if (!$locale) return;
        if (!empty($entry->getField('entity_id', $locale))) {
            $locales = $this->contentfulData->getAllISOCode();
            $tmp = [];

            foreach ($locales as $item) {
                if (!empty($entry->getField('entity_id', $item))) {
                    $tmp[$item] = $entry->getField('entity_id', $item);
                }
            }
            try {
                //TODO when create all_store entry and then delete it check how it will be correct to do
                $arrayOfEntries = array_unique($tmp);
                if (count($arrayOfEntries) == 1) {
                    $this->urlRewrites->removeUrl($object->getIdentifier());
                } elseif ($object->getStoreId()[0] == 0) {
                    $this->urlRewrites->removeUrl($object->getIdentifier());
                } else {
                    $entry->setField('entity_id', $locale, null);
                    $entry->setField('urlKey', $locale, $object->getIdentifier());
                    $entry->setField('type', $locale, self::Type);
                    $entry->setField('entity', $locale, null);
                    $entry->setField('jsonData', $locale, null);
                    $entry->update();
                    $entry->publish();
                }
            } catch (\Exception $e) {
            }

        }

    }

}

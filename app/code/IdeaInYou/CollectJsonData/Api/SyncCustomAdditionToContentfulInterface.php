<?php

namespace IdeaInYou\CollectJsonData\Api;

interface SyncCustomAdditionToContentfulInterface
{
    /**
     * @param $type
     * @param $object
     * @return mixed
     */
    public function prepareSyncContentfulData($type, $object);

    /**
     * @param $type
     * @param $object
     * @return mixed
     */
    public function prepareToDeleteContentfulData($type, $object);

}

<?php

namespace IdeaInYou\CollectJsonData\Api;

use IdeaInYou\CollectJsonData\Model\SyncToContentful;

interface SyncToContentfulInterface
{

    /**
     * @api
     * @param
     * @return SyncToContentful;
     */
    public function syncAllEntity();

    /**
     * @api
     * @param
     * $entity
     * $store_id
     * @return SyncToContentful;
     */
    public function syncAllEntityByStoreId($store_id);
}

<?php

namespace IdeaInYou\CollectJsonData\Observer\Catalog;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class ProductSaveAfter
 * @package IdeaInYou\CollectJsonData\Observer\Catalog
 */
class ProductSaveAfter implements ObserverInterface
{

    const META_DATA_PRODUCT_TITLE = "meta_data_product/meta_data/meta_title";

    const META_DATA_PRODUCT_DESCRIPTION = "meta_data_product/meta_data/meta_description";

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;
    /**
     * @var CategoryRepository
     */
    private CategoryRepository $categoryRepository;
    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;

    /**
     * @param StoreManagerInterface $storeManager
     * @param CategoryRepository $categoryRepository
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        CategoryRepository $categoryRepository,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->storeManager = $storeManager;
        $this->categoryRepository = $categoryRepository;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param Observer $observer
     * @return void
     * @throws NoSuchEntityException
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $product = $observer->getProduct();
        $this->setMetaDataToProduct($product);
    }

    /**
     * @param $_product
     * @throws NoSuchEntityException
     */
    public function setMetaDataToProduct($product){
        $storeId = $this->storeManager->getStore()->getId();
        $array = $product->getCategoryIds();
        $currentId = end($array);
        $category = $this->categoryRepository->get($currentId, $this->storeManager->getStore()->getId());
        $product->setStoreId($storeId);
        $metaTitle = $this->getMetaDataTitle();
        $metaDescription = $this->getMetaDataDescription();
        $changedTitle = str_replace("{name}", $product->getName(), $metaTitle);
        $changedDescription = str_replace(["{category}", "{name}"],
            [$category->getName(), $product->getName()], $metaDescription);
        $product->setMetaTitle($changedTitle);
        $product->setMetaDescription($changedDescription);
        $product->save();
    }

    /**
     * @return mixed
     */
    public function getMetaDataTitle(){
        return $this->scopeConfig->getValue(self::META_DATA_PRODUCT_TITLE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getMetaDataDescription(){
        return $this->scopeConfig->getValue(self::META_DATA_PRODUCT_DESCRIPTION,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}

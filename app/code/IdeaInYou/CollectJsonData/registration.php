<?php

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'IdeaInYou_CollectJsonData',
    __DIR__
);

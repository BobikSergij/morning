<?php

namespace IdeaInYou\CustomAddition\Api;

interface SliderInterface
{
    const TABLE_NAME = 'ideainyou_slider';
    const ENTITY_ID = 'entity_id';


    public function getId();

    public function setId($id);


}

<?php

namespace IdeaInYou\CustomAddition\Api;

interface CategoryGroupInterface
{
    const TABLE_NAME = 'ideainyou_categorygroup';
    const ENTITY_ID = 'entity_id';


    public function getId();

    public function setId($id);


}

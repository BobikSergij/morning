<?php

namespace IdeaInYou\CustomAddition\Api;

use IdeaInYou\CustomAddition\Model\Banner;

interface BannerRepositoryInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param Banner $banner
     * @return mixed
     */
    public function save(Banner $banner);

    /**
     * @param Banner $banner
     * @return mixed
     */
    public function delete(Banner $banner);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}

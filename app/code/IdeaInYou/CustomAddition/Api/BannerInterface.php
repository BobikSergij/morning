<?php

namespace IdeaInYou\CustomAddition\Api;

interface BannerInterface
{
    const TABLE_NAME = 'ideainyou_banners';
    const ENTITY_ID = 'entity_id';


    public function getId();

    public function setId($id);


}

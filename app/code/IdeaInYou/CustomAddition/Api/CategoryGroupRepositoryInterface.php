<?php

namespace IdeaInYou\CustomAddition\Api;

use IdeaInYou\CustomAddition\Model\CategoryGroup;

interface CategoryGroupRepositoryInterface
{
    public function getById($id);

    public function save(CategoryGroup $categoryGroup);

    public function delete(CategoryGroup $categoryGroup);

    public function deleteById($id);

    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}

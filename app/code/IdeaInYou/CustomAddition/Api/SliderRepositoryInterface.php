<?php

namespace IdeaInYou\CustomAddition\Api;

use IdeaInYou\CustomAddition\Model\Slider;

interface SliderRepositoryInterface
{
    public function getById($id);

    public function save(Slider $slider);

    public function delete(Slider $slider);

    public function deleteById($id);

    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}

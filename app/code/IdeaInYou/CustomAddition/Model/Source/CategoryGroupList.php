<?php

namespace IdeaInYou\CustomAddition\Model\Source;

use IdeaInYou\CustomAddition\Model\ResourceModel\CategoryGroup\Grid\CollectionFactory;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CollectionFactoryCategory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as CollectionFactoryProduct;

class CategoryGroupList implements \Magento\Framework\Option\ArrayInterface
{

    protected $_productCollectionFactory;
    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;


    /**
     * @var CollectionFactoryCategory
     */
    private CollectionFactoryCategory $categoryCollectionFactory;

    /**
     * @param CollectionFactoryCategory $categoryCollectionFactory
     * @param CollectionFactoryProduct $productCollectionFactory
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CollectionFactoryCategory $categoryCollectionFactory,
        CollectionFactoryProduct  $productCollectionFactory,
        CollectionFactory         $collectionFactory
    )
    {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->collectionFactory = $collectionFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
    }

    public function toOptionArray()
    {
        $categories = $this->categoryCollectionFactory->create()
            ->addAttributeToSelect('*');
        $options2 = [];

        foreach ($categories as $category) {
            $options2[] = ['label' => $category->getName(), 'value' => $category->getId()];
        }
        return $options2;
    }

}

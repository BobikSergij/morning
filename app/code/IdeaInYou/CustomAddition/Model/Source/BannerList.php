<?php

namespace IdeaInYou\CustomAddition\Model\Source;

use IdeaInYou\CustomAddition\Model\ResourceModel\Banners\Grid\CollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as CollectionFactoryProduct;

class BannerList implements \Magento\Framework\Option\ArrayInterface
{

    protected $_productCollectionFactory;
    private CollectionFactory $collectionFactory;

    /**
     * @param CollectionFactoryProduct $productCollectionFactory
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CollectionFactoryProduct $productCollectionFactory,
        CollectionFactory      $collectionFactory
    )
    {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->collectionFactory = $collectionFactory;
    }

    public function toOptionArray()
    {
        $collection = $this->_productCollectionFactory->create()
            ->addAttributeToSelect('name');

        $collection1 = $this->collectionFactory->create()->getData();

        $options = [];
        $options1 = [];
        foreach ($collection as $product) {
            $options[] = ['label' => $product->getName(), 'value' => $product->getId()];
        }

        foreach ($collection1 as $item) {
            $options1[] = ['label' => $item['title_banner'], 'value' => $item['entity_id']];
        }

        return $options1;
    }

}

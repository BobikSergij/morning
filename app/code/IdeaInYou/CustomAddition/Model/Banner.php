<?php

namespace IdeaInYou\CustomAddition\Model;

use IdeaInYou\CustomAddition\Api\BannerInterface;
use Magento\Framework\DataObject\IdentityInterface;

class Banner extends \Magento\Framework\Model\AbstractModel implements BannerInterface, IdentityInterface
{
    const  CACHE_TAG = 'ideainyou_banners';

    protected $_eventPrefix = 'ideainyou_banners';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel\Banner::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * @param $id
     * @return Banner
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }


}

<?php

namespace IdeaInYou\CustomAddition\Model;

use IdeaInYou\CustomAddition\Api\CategoryGroupInterface;
use Magento\Framework\DataObject\IdentityInterface;

class CategoryGroup extends \Magento\Framework\Model\AbstractModel implements CategoryGroupInterface, IdentityInterface
{
    const  CACHE_TAG = 'ideainyou_categorygroup';

    protected $_eventPrefix = 'ideainyou_categorygroup';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel\CategoryGroup::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * @param $id
     * @return CategoryGroup
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }


}

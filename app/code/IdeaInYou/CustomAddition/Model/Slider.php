<?php

namespace IdeaInYou\CustomAddition\Model;

use IdeaInYou\CustomAddition\Api\SliderInterface;
use Magento\Framework\DataObject\IdentityInterface;

class Slider extends \Magento\Framework\Model\AbstractModel implements SliderInterface, IdentityInterface
{
    const  CACHE_TAG = 'custom_addition';

    protected $_eventPrefix = 'custom_addition';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\IdeaInYou\CustomAddition\Model\ResourceModel\Slider::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * @param $id
     * @return Slider
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }


}

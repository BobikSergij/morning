<?php

namespace IdeaInYou\CustomAddition\Model\Banners;

use IdeaInYou\CustomAddition\Model\Banners\BannerCollection;


class BannerSave
{
    /**
     * @var \IdeaInYou\CustomAddition\Model\Banners\BannerCollection
     */
    private \IdeaInYou\CustomAddition\Model\Banners\BannerCollection $bannerCollection;

    /**
     * @param BannerCollection $bannerCollection
     */
    public function __construct(
        BannerCollection $bannerCollection
    ) {

        $this->bannerCollection = $bannerCollection;
    }

    /**
     * @param $type
     * @param $store
     * @return void
     */
    public function saveAllEntity($type, $store){
        $this->bannerCollection->saveAllBannersToContentful($type, $store);
    }

}

<?php

namespace IdeaInYou\CustomAddition\Model\ResourceModel;

use IdeaInYou\CustomAddition\Api\BannerInterface;

class Banner extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init(BannerInterface::TABLE_NAME, BannerInterface::ENTITY_ID);
    }
}

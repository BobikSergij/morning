<?php

namespace IdeaInYou\CustomAddition\Model\ResourceModel\CategoryGroup;

use IdeaInYou\CustomAddition\Model\CategoryGroup;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            CategoryGroup::class,
            \IdeaInYou\CustomAddition\Model\ResourceModel\CategoryGroup::class
        );
    }
}

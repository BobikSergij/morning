<?php

namespace IdeaInYou\CustomAddition\Model\ResourceModel;

use IdeaInYou\CustomAddition\Api\CategoryGroupInterface;

class CategoryGroup extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init(CategoryGroupInterface::TABLE_NAME, CategoryGroupInterface::ENTITY_ID);
    }
}

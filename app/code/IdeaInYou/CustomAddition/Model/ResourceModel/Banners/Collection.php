<?php

namespace IdeaInYou\CustomAddition\Model\ResourceModel\Banners;

use IdeaInYou\CustomAddition\Model\Banner;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Banner::class,
            \IdeaInYou\CustomAddition\Model\ResourceModel\Banner::class
        );
    }
}

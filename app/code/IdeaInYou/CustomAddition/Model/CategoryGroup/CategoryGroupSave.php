<?php

namespace IdeaInYou\CustomAddition\Model\CategoryGroup;

class CategoryGroupSave
{
    /**
     * @var \IdeaInYou\CustomAddition\Model\CategoryGroup\CategoryGroupCollection
     */
    private \IdeaInYou\CustomAddition\Model\CategoryGroup\CategoryGroupCollection $categoryGroupCollection;

    /**
     * @param CategoryGroupCollection $categoryGroupCollection
     */
    public function __construct(
        CategoryGroupCollection $categoryGroupCollection
    ) {
        $this->categoryGroupCollection = $categoryGroupCollection;
    }

    /**
     * @param $type
     * @param $store
     * @return void
     */
    public function saveAllEntity($type, $store){
        $this->categoryGroupCollection->saveAllCatGroupToContentful($type, $store);
    }

}

<?php

namespace IdeaInYou\CustomAddition\Plugin\Banner;

use IdeaInYou\CollectJsonData\Api\ValidateContentfulInterface;
use IdeaInYou\CollectJsonData\Api\SyncCustomAdditionToContentfulInterface;
use IdeaInYou\CustomAddition\Model\BannerRepository;
use Magento\Framework\Model\AbstractModel;

class BannerDelete
{
    /**
     *
     */
    const TYPE = 'banner';

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @var SyncCustomAdditionToContentfulInterface
     */
    private SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     * @param SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful
     */
    public function __construct(
        ValidateContentfulInterface    $validateContentful,
        SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful
    ) {
        $this->validateContentful = $validateContentful;
        $this->syncCustomAdditionToContentful = $syncCustomAdditionToContentful;
    }

    /**
     * @param BannerRepository $subject
     * @param $result
     * @param AbstractModel $object
     * @return mixed
     */
    public function afterDelete(BannerRepository $subject, $result, AbstractModel $object)
    {
        if (!$this->validateContentful->isEnable()) return $result;
        $this->syncCustomAdditionToContentful->prepareToDeleteContentfulData(self::TYPE, $object);
        return $result;
    }

}

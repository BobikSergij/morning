<?php

namespace IdeaInYou\CustomAddition\Plugin\Banner;

use IdeaInYou\CollectJsonData\Api\ValidateContentfulInterface;
use IdeaInYou\CustomAddition\Model\BannerRepository;

class BannerBeforeDelete
{
    const TYPE = 'banner';
    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful
    ) {
        $this->validateContentful = $validateContentful;
    }

    /**
     * @param BannerRepository $subject
     * @return void
     */
    public function beforeDelete(BannerRepository $subject)
    {
        $this->validateContentful->validator(self::TYPE);
    }
}

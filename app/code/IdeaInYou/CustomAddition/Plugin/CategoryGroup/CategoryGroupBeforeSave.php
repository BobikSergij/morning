<?php

namespace IdeaInYou\CustomAddition\Plugin\CategoryGroup;

use IdeaInYou\CollectJsonData\Api\ValidateContentfulInterface;
use IdeaInYou\CustomAddition\Model\CategoryGroupRepository;

class CategoryGroupBeforeSave
{
    const TYPE = 'categoryGroup';
    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful
    ) {
        $this->validateContentful = $validateContentful;
    }

    /**
     * @param CategoryGroupRepository $subject
     * @return void
     */
    public function beforeSave(CategoryGroupRepository $subject)
    {
        $this->validateContentful->validator(self::TYPE);
    }
}

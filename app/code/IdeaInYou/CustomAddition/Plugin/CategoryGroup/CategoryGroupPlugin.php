<?php

namespace IdeaInYou\CustomAddition\Plugin\CategoryGroup;

use IdeaInYou\CollectJsonData\Api\ValidateContentfulInterface;
use IdeaInYou\CustomAddition\Controller\Adminhtml\CategoryGroup\Save;
use IdeaInYou\CollectJsonData\Api\SyncCustomAdditionToContentfulInterface;
use IdeaInYou\CustomAddition\Model\CategoryGroupRepository;
use Magento\Framework\Model\AbstractModel;

class CategoryGroupPlugin
{
    /**
     *
     */
    const TYPE = 'categoryGroup';

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @var SyncCustomAdditionToContentfulInterface
     */
    private SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     * @param SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful,
        SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful
    ) {
        $this->validateContentful = $validateContentful;
        $this->syncCustomAdditionToContentful = $syncCustomAdditionToContentful;
    }

    /**
     * @param CategoryGroupRepository $subject
     * @param $result
     * @param AbstractModel $object
     * @return mixed
     */
    public function afterSave(CategoryGroupRepository $subject, $result, AbstractModel $object)
    {
        if (!$this->validateContentful->isEnable()) return $result;
        $this->syncCustomAdditionToContentful->prepareSyncContentfulData(self::TYPE , $object);
        return $result;
    }

}

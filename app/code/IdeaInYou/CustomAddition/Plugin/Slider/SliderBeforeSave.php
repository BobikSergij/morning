<?php

namespace IdeaInYou\CustomAddition\Plugin\Slider;

use IdeaInYou\CollectJsonData\Api\ValidateContentfulInterface;
use IdeaInYou\CustomAddition\Model\SliderRepository;

class SliderBeforeSave
{
    const TYPE = 'slider';
    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful
    ) {
        $this->validateContentful = $validateContentful;
    }

    /**
     * @param SliderRepository $subject
     * @return void
     */
    public function beforeSave(SliderRepository $subject)
    {
        $this->validateContentful->validator(self::TYPE);
    }
}

<?php

namespace IdeaInYou\CustomAddition\Plugin\Slider;

use IdeaInYou\CollectJsonData\Api\ValidateContentfulInterface;
use IdeaInYou\CollectJsonData\Api\SyncCustomAdditionToContentfulInterface;
use IdeaInYou\CustomAddition\Model\SliderRepository;
use Magento\Framework\Model\AbstractModel;

class SliderPlugin
{
    /**
     *
     */
    const TYPE = 'slider';

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @var SyncCustomAdditionToContentfulInterface
     */
    private SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     * @param SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful,
        SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful
    ) {
        $this->validateContentful = $validateContentful;
        $this->syncCustomAdditionToContentful = $syncCustomAdditionToContentful;
    }

    /**
     * @param SliderRepository $subject
     * @param $result
     * @param AbstractModel $object
     * @return mixed
     */
    public function afterSave(\IdeaInYou\CustomAddition\Model\SliderRepository $subject, $result, AbstractModel $object)
    {
        if (!$this->validateContentful->isEnable()) return $result;
        $this->syncCustomAdditionToContentful->prepareSyncContentfulData(self::TYPE , $object);
        return $result;
    }

}


<?php

namespace IdeaInYou\CheckoutGraphQl\Api;

interface ConectionToUkrPostInterface
{

    /**
     * @return \IdeaInYou\CheckoutGraphQl\Model\UkrPoshta\ConectionToUkrPost
     *@api
     */
    public function getRegions();

    /**
     * @param string $region_id
     * @return \IdeaInYou\CheckoutGraphQl\Model\UkrPoshta\ConectionToUkrPost
     *@api
     */
    public function getDistrict($region_id);

    /**
     * @param string $city
     * @return \IdeaInYou\CheckoutGraphQl\Model\UkrPoshta\ConectionToUkrPost
     *@api
     */
    public function getCity($city);

    /**
     * @param string $city_id
     * @return \IdeaInYou\CheckoutGraphQl\Model\UkrPoshta\ConectionToUkrPost
     *@api
     */
    public function getPostofficeByCity($city_id);

}

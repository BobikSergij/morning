<?php

namespace IdeaInYou\CheckoutGraphQl\Api;

interface GetDataInterface
{

    /**
     * @return mixed
     */
    public function getApiKey();

    /**
     * @api
     * @param string $findByString
     * @return \IdeaInYou\CheckoutGraphQl\Helper\GetData
     */
    public function getCities($findByString);

    /**
     * @param $city_id
     * @return void
     */
    public function getWarehouses($city_id);

    /**
     * @api
     * @param string $searchStringArray
     * @return \IdeaInYou\CheckoutGraphQl\Helper\GetData
     */
    public function findNearestWarehouse($searchStringArray);
}
